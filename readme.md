# Overview
This Python scripts will help you to parse Confluence/JIRA application logs or `catalina.out` file into individual file(s) that contains the logs from each startup.

# Pre-requisites for the Script
1. Windows users:
	1. Install Python 2.7.6 from [Python 2.7.6 Download site](https://www.python.org/download/releases/2.7.6/).
	2.  Check your **Path** env variable if `Python installation folder`; i.e: `C:\Python27` is included, if it's not please include the path and reopen your CMD.
2.  Ubuntu/MAC user:
	1. Python is pre-installed.

# Script Configuration & Installation
1. Clone the repo.
2.  If you are using Ubuntu/MAC, you may create an alias to execute the script anywhere in the OS.
3.  If you are using Windows, then you will have to `cd` to the folder the Pyhton script located. (I might find a way to make this easier) (fingercrossed)

# Usage

Run the scripts with the `-h` parameter to see the the available arguments, for example `python log-summarizer.py -h`.
```
usage: log-summarizer.py [-h] support_zip {jira,confluence}

Log summarizer.

positional arguments:
  support_zip        Support ZIP filename
  {jira,confluence}  Atlassian Product

optional arguments:
  -h, --help         show this help message and exit
```

This Python script can handle 4 types of input:

* Processing **Support ZIP compressed file**:
	
	`python log-summarizer.py JIRA_support_2017-03-13-15-04-22.zip jira`

* Processing **Support ZIP extracted file**:

	`python log-summarizer.py JIRA_support_2017-03-13-15-04-22 jira`

* Processing **catalina.out** file:

	`python log-summarizer.py catalina.out jira`

* Processing **atlassian-jira.log** file:

	`python log-summarizer.py atlassian-jira.log jira`

# Feedback, Bugs and New Features
Please raise any feedback, bugs and new features in the [issue tracker](https://bitbucket.org/mogavenasan/log-summarizer/issues?status=new&status=open).